<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|T
*/

Route::get('/', function () {
    return view('welcome');
});

//get all
Route::get('/boping-trans', 'SummmarizeController@bopingTrans')->name('history.bopingTrans');
Route::post('/filter-boping-trans', 'SummmarizeController@filterTransBoping')->name('filter.filterTransBoping');

Route::get('/summarize', 'SummmarizeController@index')->name('filter.allSumWinLost');

//get data by filter
Route::post('/summarize-filter', 'SummmarizeController@searchDataByFilter')->name('filter.sumWinLost');
Route::get('/details-summarize/{id}', 'SummmarizeController@detailTransBoping')->name('filter.detailTrans');

//observer
Route::get('/test-observer', 'SummmarizeController@userObserver')->name('obs.userObserver');


