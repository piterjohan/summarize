<?php

use bfinlay\SpreadsheetSeeder\SpreadsheetSeeder;

class SportTableSeeder extends SpreadsheetSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->worksheetTableMapping = ['Sheet1' => 'first_table', 'Sheet2' => 'second_table'];
        parent::run();
    }
}
