<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SumWinlost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sum_winlost', function (Blueprint $table) {
            $table->id();
            $table->dateTime('date_trans')->default(null);
            $table->string('vendor_member_id',30);
            $table->decimal('total', 10,2);
            $table->decimal('win', 10,2);
            $table->decimal('lost', 10,2);
            $table->decimal('winlost_report', 10,2);
            $table->integer('sport_type')->nullable()->default(0);
            $table->timestamps();

            $table->foreign('vendor_member_id')->references('vendor_member_id')->on('player');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sum_winlost');
    }
}
