<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BopingTrans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boping_trans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('trans_id')->unique();
            $table->string('vendor_member_id', 30);
            $table->string('operator_id', 50);
            $table->integer('league_id')->nullable()->default(0);
            $table->mediumText('leaguename')->nullable();
            $table->integer('match_id');
            $table->integer('home_id')->nullable();
            $table->mediumText('hometeamname')->nullable();
            $table->integer('away_id')->nullable();
            $table->mediumText('awayteamname');
            $table->integer('team_id')->nullable();
            $table->string('game_id', 50)->nullable();
            $table->dateTime('match_datetime')->nullable()->default(null);
            $table->integer('sport_type')->nullable()->default(0);
            $table->integer('bet_type');
            $table->unsignedBigInteger('parlay_ref_no')->nullable()->default(0);
            $table->decimal('odds', 10, 2);
            $table->decimal('stake', 10, 2);
            $table->dateTime('transaction_time');
            $table->string('ticket_status', 15);
            $table->decimal('winlost_amount', 10, 2);
            $table->decimal('after_amount', 10, 2)->nullable();
            $table->integer('currency');
            $table->dateTime('winlost_datetime');
            $table->tinyInteger('odds_type');
            $table->string('odds_info', 50);
            $table->string('isLucky', 5)->nullable();
            $table->string('bet_team', 100)->nullable();
            $table->mediumText('exculding')->nullable();
            $table->mediumText('bet_tag')->nullable();
            $table->decimal('home_hdp', 10, 2)->nullable()->default(0);
            $table->decimal('away_hdp', 10, 2)->nullable()->default(0);
            $table->decimal('hdp', 10, 2)->nullable()->default(0);;
            $table->decimal('ou_hdp', 10, 2)->nullable();
            $table->string('betfrom', 5);
            $table->string('islive', 5);
            $table->smallInteger('home_score')->nullable()->default(0);
            $table->smallInteger('home_score2')->nullable();
            $table->smallInteger('away_score')->nullable()->default(0);
            $table->smallInteger('away_score2')->nullable();
            $table->dateTime('settlement_time')->nullable()->default(null);
            $table->string('customInfo1', 200)->nullable();
            $table->string('customInfo2', 200)->nullable();
            $table->string('customInfo3', 200)->nullable();
            $table->string('customInfo4', 200)->nullable();
            $table->string('customInfo5', 200)->nullable();
            $table->string('ba_status', 1);
            $table->smallInteger('esports_gameid')->nullable();
            $table->smallInteger('total_score')->nullable();
            $table->unsignedBigInteger('version_key')->unique();
            $table->mediumText('parlayData')->nullable();
            $table->mediumText('winlost')->nullable();
            $table->timestamps();

            $table->foreign('bet_type')->references('bet_id')->on('bet_type');
            $table->foreign('vendor_member_id')->references('vendor_member_id')->on('player');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boping_trans');
    }
}
