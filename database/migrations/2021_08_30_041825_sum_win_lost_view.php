<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class SumWinLostView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        DB::statement("
//           CREATE VIEW sum_win_lost_view AS
//      (
//          SELECT
//            bt.winlost_datetime,
//            bt.vendor_member_id,
//            st.sport_name,
//            SUM(bt.stake) as omzet,
//            SUM(CASE WHEN bt.winlost_amount > 0 THEN bt.winlost_amount ELSE 0 END) AS win,
//            SUM(CASE WHEN bt.winlost_amount < 0 THEN bt.winlost_amount ELSE 0 END) as lost,
//            SUM(bt.winlost_amount) as winlost_report
//          FROM `boping_trans` as bt
//
//          INNER JOIN `sport_type` as st on bt.sport_type = st.sport_id
//          Group BY bt.winlost_datetime, bt.vendor_member_id, bt.sport_type
//      )
//      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS sum_win_lost_view');
    }
}
