<?php

namespace Tests\Feature;

use App\Model\BopingTransModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BopingTest extends TestCase
{

//    use RefreshDatabase;

    /** @test */
    public function call_api_get_bet_detail()
    {
        $response = $this->get('api/boping');

        $response->assertJson(["data" => "Recorded"]);

    }

    /** @test */
    public function api_get_summarize()
    {
        $response = $this->get('api/summarize');

        $response->assertJson(["message" => "ok"]);
    }

    /** @test */
    public function web_get_test_user_observer()
    {
        $response = $this->get('/test-observer');

        $response->dump();
        $response->assertSee('Observer User Success');
    }

    /** @test */
    public function web_get_summarize()
    {
        $response = $this->get('/summarize');

        $response->dump();
        $response->assertSee('bro_MALAMPERTAMA_test');
    }

    /**
     * #table
     * date |win|lose|omzet
     * sport_type |win|lose|omzet
     * player ngikut sport_type |win|lose|omset
     * @test
     */
    public function web_get_summarize_details()
    {
        $response = $this->get('summarize-details');

        $response->assertSee('bro_MALAMPERTAMA_test');

    }


    /** @test */
    public function summarize_with_all_filter()
    {
        $response = $this->post('/summarize-filter',[
            'date' => '21-06-2021',
            'sport_type' => '1',
            'player_data' => 'bro_MALAMPERTAMA_test'
        ]);

        $response->assertSee('bro_MALAMPERTAMA_test');
    }

    /** @test */
    public function summarize_filter_with_date_and_player_data()
    {
        $response = $this->post('/summarize-filter',[
            'date' => '21-06-2021',
            'sport_type' => '',
            'player_data' => 'bro_MALAMPERTAMA_test'
        ]);

        $response->assertSee('bro_MALAMPERTAMA_test');
    }

    /** @test */
    public function summarize_filter_with_date()
    {
        $response = $this->post('/summarize-filter',[
            'date' => '21-06-2021',
            'sport_type' => '',
            'player_data' => ''
        ]);

        $response->assertSee('bro_MALAMPERTAMA_test');
    }

    /** @test */
    public function filter_boping_trans_with_date()
    {
        $response = $this->post('/filter-boping-trans',[
            'date' => '',
            'sport_type' => '',
            'player_data' => 'bro_MALAMPERTAMA_test'
        ]);

        $response->assertSee('bro_MALAMPERTAMA_test');
    }

    /** @test */
    public function detail_summary_boping()
    {
        $id = 'bro_TESTKEL123_test';
        $response = $this->get('/details-summarize/'.$id);

        $response->assertSeeText($id);
    }
}
