@extends('layouts.html_layout')

@section('title', 'Summarize Report')

@section('css-custom')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.0/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('content')

    <form action="{{ route('filter.sumWinLost') }}" method="POST">
        @csrf
        <input type="hidden" name="csrf-token" value="{{ csrf_token() }}">
        <p>Date</p>
        <input type="text" name="date">
        <p>Sport Type</p>
        <select name="sport_type">
            <option value=""> -- Sport Type --</option>
            @foreach($sport_type as $sport_data)
                <option value="{{ $sport_data->sport_id }}"> {{ $sport_data->sport_name }}</option>
            @endforeach
        </select>
        <p>Player</p>
        <select name="player_data">
            <option value=""> -- Player --</option>
            @foreach($player as $player_data)
                <option value="{{ $player_data->vendor_member_id }}"> {{ $player_data->vendor_member_id }}</option>
            @endforeach
        </select>
        <button type="submit">Search</button>
        <a href="{{ route('filter.allSumWinLost') }}">All-Data</a>
    </form>

    <table id="summarize" class="display" style="width:100%">
        <thead>
        <tr>
            <th> Date Transaction</th>
            <th> Member</th>
            <th> Omzet</th>
            <th> Win Amount</th>
            <th> Lost Amount</th>
            <th> Win Lost Total</th>
            <th> Sport Type</th>
            <th> Details Trans</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $value)
            <tr>
                <td>{{ $value->date_trans }}</td>
                <td>{{ $value->vendor_member_id }}</td>
                <td>{{ $value->omzet }}</td>
                <td>{{ $value->win }}</td>
                <td>{{ $value->lost }}</td>
                <td>{{ $value->winlost_report }}</td>
                <td>
                    @foreach($value->sportType as $sport)
                            {{ $sport->sport_name }}
                    @endforeach
               </td>
                <td>
                    <a href="{{ route('filter.detailTrans',$value->vendor_member_id) }}">Details</a>
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2" style="text-align: right">
                <b><h3>Total</h3></b>
            </td>
            <td id="win_amount"></td>
            <td id="lost_amount"></td>
            <td id="winlost_amount"></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tfoot>
    </table>
@endsection

@section('js-custom')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script>
        $('input[name="date"]').daterangepicker();
    </script>
    <script>
        $(document).ready(function () {
            $('#summarize').DataTable({
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function (i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                                i : 0;
                    }

                    // win_amount over all pages
                    omzet_total = api
                        .column(2)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // win_amount over this page
                    omzet_current = api
                        .column(2, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // win_amount over all pages
                    win_amount_total = api
                        .column(3)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // win_amount over this page
                    win_amount_current = api
                        .column(3, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // lost_amount over all pages
                    lost_amount_total = api
                        .column(4)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // win_amount over this page
                    lost_amount_current = api
                        .column(4, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // lost_amount over all pages
                    winlost_amount_total = api
                        .column(5)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // win_amount over this page
                    winlost_amount_current = api
                        .column(5, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Update footer
                    $(api.column(2).footer()).html(
                        omzet_current + ' ( Total: ' + omzet_total + ')'
                    );
                    $(api.column(3).footer()).html(
                        win_amount_current + ' ( Total: ' + win_amount_total + ')'
                    );
                    $(api.column(4).footer()).html(
                        lost_amount_current + ' ( Total: ' + lost_amount_total + ')'
                    );
                    $(api.column(5).footer()).html(
                        winlost_amount_current + ' ( Total: ' + winlost_amount_total + ')'
                    );

                }
            });
        });
    </script>
@endsection
