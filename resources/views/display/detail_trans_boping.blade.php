@extends('layouts.html_layout')

@section('title', 'Detail Boping Transaction')

@section('css-custom')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.0/datatables.min.css"/>
@endsection

@section('content')

    <div>
        <a href="{{ route('history.bopingTrans') }}">Back</a>
    </div>
    <br>
    <br>


    <table id="boping-trans" class="display" style="width:100%">
        <thead>
        <tr>
            <th> Date Transaction</th>
            <th> Member</th>
            <th> Stake </th>
            <th> Win Lost Amount</th>
            <th> After Amount</th>
            <th> Status</th>
            <th> Sport Type</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $value)
            <tr>
                <td>{{ $value->transaction_time }}</td>
                <td>{{ $value->vendor_member_id }}</td>
                <td>{{ $value->stake }}</td>
                <td>{{ $value->winlost_amount }}</td>
                <td>{{ $value->after_amount }}</td>
                <td>{{ $value->ticket_status }}</td>
                <td>
                    @foreach($value->sportType as $sport_type)
                        {{ (isset($sport_type->sport_name) ? $sport_type->sport_name : "None") }}
                    @endforeach
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2" style="text-align: right">
                <b><h3>Total</h3></b>
            </td>
            <td id="stake_amount"></td>
            <td id="winlost_amount"></td>
            <td id="after_amount"></td>
            <td></td>
            <td></td>
        </tr>
        </tfoot>
    </table>

@endsection

@section('js-custom')

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#boping-trans').DataTable({
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // stake over all pages
                    stake_total = api
                        .column( 2 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // stake over this page
                    stake_current = api
                        .column( 2, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // lost_amount over all pages
                    winlost_amount_total = api
                        .column( 3 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // win_amount over this page
                    winlost_amount_current = api
                        .column( 3, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 2 ).footer() ).html(
                        stake_current +' ( Total: '+ stake_total + ')'
                    );
                    $( api.column( 3 ).footer() ).html(
                        winlost_amount_current +' ( Total: '+ winlost_amount_total + ')'
                    );

                }
            });
        } );

        // total_stake_amount = 0;
        // total_winlost_amount = 0;
        // total_after_amount = 0;
        //
        // $('#boping-trans tr').each(function() {
        //     if (!this.rowIndex) return; // skip first row
        //     let stake_amount = this.cells[2].innerHTML;
        //     let winlost_amount = this.cells[3].innerHTML;
        //     let after_amount = this.cells[4].innerHTML;
        //
        //     total_stake_amount += Number(stake_amount);
        //     total_winlost_amount += Number(winlost_amount);
        //     total_after_amount += Number(after_amount);
        // });
        //
        // document.getElementById('stake_amount').innerText = total_stake_amount;
        // document.getElementById('winlost_amount').innerText = total_winlost_amount;
        // document.getElementById('after_amount').innerText = total_after_amount;
    </script>
@endsection
