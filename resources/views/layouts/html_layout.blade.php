<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    {{-- Css --}}
    @yield('css-custom')
</head>
<body>

    @yield('content')

    {{-- JS --}}
    @yield('js-custom')
</body>
</html>
