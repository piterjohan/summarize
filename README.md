# How to Use

Below step by step to using this app:

1. git clone git@gitlab.com:piterjohan/summarize.git

2. composer install 
    > composer install

3. copy file .env.example & rename to .env

4. Edit some of configuration like below
    ``` 
       DB_CONNECTION=mysql
       DB_HOST=127.0.0.1
       DB_PORT=3306
       DB_DATABASE= your database name
       DB_USERNAME= your db username
       DB_PASSWORD= your password

       QUEUE_CONNECTION=database
    ```
   
5. Migrate database with seed
   > php artisan migrate --seed
   
6. Running Jobs Queue
   > php artisan queue:work --queue=boping

7. Run artisan with port 8181, Rescheduler depends on port 8181
   > php artisan serve --port=8181

# Rescheduler support
 you can check the command Resecheduler from
 ``app\Console\Kernel.php``


1. Fetching Boping Trans, Sum WinLost, Save Player from terminal
   > php artisan boping:daily
   > 
   File command boping:daily
   `` app\Console\Commands\FetchBoping``
