<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PlayerModel extends Model
{
    protected $primaryKey = 'id';

    protected $table = "player";

    protected $fillable = ['vendor_member_id'];

    public $timestamps = false;

    /** yout docs block */
    public function sumWinLosePlayer()
    {
        return $this->belongsTo('App\Model\SumWinLoseModel', 'vendor_member_id', 'vendor_member_id');
    }
}
