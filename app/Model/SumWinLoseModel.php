<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SumWinLoseModel extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'sum_winlost';

    protected $fillable = [
            'date_trans',
            'vendor_member_id',
            'total',
            'win',
            'lost',
            'winlost_report',
            'sport_type',
        ];

    public $timestamps = false;


    /**
     * Relation sumWin to player
     */
    public function player()
    {
        return $this->hasMany('App\Model\PlayerModel', 'vendor_member_id', 'vendor_member_id');
    }

    /** yout docs block */
    public function sportType()
    {
        return $this->hasMany('App\Model\SportTypeModel', 'sport_id', 'sport_type');
    }
}
