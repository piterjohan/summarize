<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BetTypeModel extends Model
{
    protected $primaryKey = "id";
    protected $table = "bet_type";

    protected $fillable = ['bet_id', 'type_name'];

    public $timestamps = false;

    /*
     * Bet type to booping transmodel
     */
    public function boopingBetType()
    {
        return $this->belongsTo('App\Model\BopingTransModel', 'bet_type');
    }
}
