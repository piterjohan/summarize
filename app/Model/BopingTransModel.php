<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BopingTransModel extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'boping_trans';

    protected $fillable = [
        'trans_id',
        'vendor_member_id',
        'operator_id',
        'league_id',
        'leaguename',
        'match_id',
        'home_id',
        'hometeamname',
        'away_id',
        'awayteamname',
        'team_id',
        'game_id',
        'match_datetime',
        'sport_type',
        'bet_type',
        'parlay_ref_no',
        'odds',
        'stake',
        'transaction_time',
        'ticket_status',
        'winlost_amount',
        'after_amount',
        'currency',
        'winlost_datetime',
        'odds_type',
        'odds_info',
        'isLucky',
        'bet_team',
        'exculding',
        'bet_tag',
        'home_hdp',
        'away_hdp',
        'hdp',
        'ou_hdp',
        'betfrom',
        'islive',
        'home_score',
        'home_score2',
        'away_score',
        'away_score2',
        'settlement_time',
        'customInfo1',
        'customInfo2',
        'customInfo3',
        'customInfo4',
        'customInfo5',
        'ba_status',
        'esports_gameid',
        'total_score',
        'version_key',
        'parlayData',
        'winlost',
    ];

    public $timestamps = false;


    public function betType()
    {
        return $this->hasMany('App\Model\BetTypeModel', 'bet_id');
    }

    public function sportType()
    {
        return $this->hasMany('App\Model\SportTypeModel', 'sport_id');
    }
}
