<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SumWinlostViewModel extends Model
{
    protected $table = 'sum_win_lost_view';
}
