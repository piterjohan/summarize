<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VersionKeyModel extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'version_key';

    protected $fillable = ['version_key'];

}
