<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SportTypeModel extends Model
{
    protected $primaryKey = "id";
    protected $table = "sport_type";

    protected $fillable = ['sport_id', 'sport_name', 'sport_group'];

    public $timestamps = false;

    /*
     * Sport type to booping transmodel
     */
    public function sportTypeBooping()
    {
        return $this->belongsTo('App\Model\BopingTransModel', 'sport_type');
    }

    /** yout docs block */
    public function sumWinLoseSportType()
    {
        return $this->belongsTo('App\Model\SumWinLoseModel', 'sport_type', 'sport_id');
    }
}
