<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class FetchBoping extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'boping:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetching Boping Trans And Sum WinLost';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $http = Http::get('http://127.0.0.1:8181/api/boping');

        $this->info('fetch boping and sumwin lost success');
    }
}
