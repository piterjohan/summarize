<?php

namespace App\Http\Controllers;

use App\Model\BetTypeModel;
use App\Model\BopingTransModel;
use App\Model\PlayerModel;
use App\Model\SportTypeModel;
use App\Model\SumWinLoseModel;
use App\Model\SumWinLostViewModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SummmarizeController extends Controller
{
    /** observer user */
    public function userObserver()
    {
        $random_name = Str::random(6);
        $random_email = Str::random(12);

        $users = User::create([
            'name' => $random_name,
            'email' => $random_email.'@gmail.com',
            'password' => '1234567'
        ]);

        dd($users);
        return 'Observer User Success';
    }

    public function index()
    {
        // use Cache + View for sum winlost summary
        if ( !Cache::get('sumwin_lost_view'))
        {
            Cache::put('sumwin_lost_view', $this->summaryWinLost(), now()->addMinutes(5));
        }

        return view('display/summarize_report',[
            'sport_type' => $this->sportType(),
            'player' => $this->playerData(),
            'data' => ( Cache::has('sumwin_lost_view') ) ? Cache::get('sumwin_lost_view') : $this->summaryWinLost(),
        ]);
    }

    /**
     * Search data sum win lost
     * @param  Request $request
     * Query filter
     * @param date $request->date
     * @param string $request->sport_type base on sport_type table sport_id
     * @param string $request->player base on plyaer table vendor_member_id
     */
    public function searchDataByFilter(Request $request)
    {
        // filter by date
        $split_date = explode(" - ", $request->date);

        $reverse_date_start = date("Y-m-d", strtotime($split_date[0]));
        $reverse_date_end = date("Y-m-d", strtotime($split_date[1]));

        $filter_by_date = SumWinLoseModel::with('sportType')
                        ->whereDate('date_trans', '>=', $reverse_date_start)
                        ->whereDate('date_trans', '<=', $reverse_date_end)
                        ->orWhere('sport_type', $request->sport_type)
                        ->orWhere('vendor_member_id', $request->player_data)
                        ->get();

//        dd($request->all(), $split_date, $reverse_date_start, $reverse_date_end, $filter_by_date);

        return view('display/summarize_report',[
            'sport_type' => $this->sportType(),
            'player' => $this->playerData(),
            'data' => $filter_by_date,
        ]);
    }

    /**
     * web.php details-summarize/{id}
     * @param int $id
     */
    public function detailTransBoping($id)
    {
        //get all transaction booping
        $boping_trans = BopingTransModel::with('sportType')
                        ->where('vendor_member_id', $id)
                        ->get();
//        dd($boping_trans);

        return view('display.detail_trans_boping',[
            'data' => $boping_trans,
        ]);
    }

    /**
     * web.php bopingTrans
     */
    public function bopingTrans()
    {
//         Cache
        if ( !Cache::get('boping_trans') )
        {
            Cache::put('boping_trans', $this->bopingTransData(), now()->addMinute(5));
        }

        return view('display.trans_boping',[
            'sport_type' => $this->sportType(),
            'bet_type' => $this->betType(),
            'player' => $this->playerData(),
            'data' => ( Cache::has('boping_trans')) ? Cache::get('boping_trans') : $this->bopingTransData(),
        ]);
    }

    /**
     * web.php filter-boping-trans/{id}
     * Search data sum win lost
     * @param  Request $request
     * Query filter
     * @param date $request->date
     * @param string $request->sport_type base on sport_type table sport_id
     * @param string $request->bet_type base on bet_type table bet_id
     * @param string $request->player base on plyaer table vendor_member_id
     */
    public function filterTransBoping(Request $request)
    {
        // filter by date
        $split_date = explode(" - ", $request->date);

        $reverse_date_start = date("Y-m-d", strtotime($split_date[0]));
        $reverse_date_end = date("Y-m-d", strtotime($split_date[1]));

        $boping_trans = BopingTransModel::with('sportType','betType')
            ->whereDate('winlost_datetime', '>=' ,$reverse_date_start)
            ->whereDate('winlost_datetime', '<=' ,$reverse_date_end)
            ->orWhere('sport_type', $request->sport_type)
            ->orWhere('vendor_member_id', $request->player_data)
            ->get();

        return view('display.trans_boping',[
            'sport_type' => $this->sportType(),
            'bet_type' => $this->betType(),
            'player' => $this->playerData(),
            'data' => $boping_trans,

        ]);
    }

    /** boping Trans Data */
    public function bopingTransData()
    {
        return BopingTransModel::with('sportType', 'betType')->get();
    }

    /**
     * Data Sport Type
     */
    protected function sportType()
    {
        return SportTypeModel::all();
    }

    /** player Data */
    protected function playerData()
    {
        return PlayerModel::all();
    }

    /** bet Data */
    protected function betType()
    {
        return BetTypeModel::all();
    }

    /**
     * report all summary
     */
    public function summaryWinLost()
    {
        return SumWinLoseModel::with('sportType')->get();
    }
}
