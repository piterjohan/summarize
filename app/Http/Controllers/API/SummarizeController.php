<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Model\SumWinLoseModel;
use Illuminate\Support\Facades\DB;

class SummarizeController extends Controller
{


    /**
     * Original Summarize from database sumwin
     */
    public function index()
    {
        $data = SumWinLoseModel::with('player', 'sportType')->get();

        return response()->json(['message' => 'ok', 'data' => $data], 200);
    }

    /**
     * Grouping by date,
     * then add key inside date: sport type,
     * then inside key each sport type have player
     */
    public function groupingWithEachParameter()
    {
        //date
        $dates = SumWinLoseModel::select(DB::raw('date_trans,
                    SUM(total) as omzet,
                    SUM(CASE WHEN win>0 THEN win ELSE 0 END) as win,
                    SUM(CASE WHEN lost<0 THEN lost ELSE 0 END) as lost,
                    SUM(winlost_report) as winlose_report
                    '))->groupBy('date_trans')
                    ->get();

        foreach ($dates as $index_date => $date)
        {
            //sport_type
            $sport_type = SumWinLoseModel::with('sportType')
                ->whereDate('date_trans', $date->date_trans)
                ->select(DB::raw(
                    'sport_type,
                    SUM(total) as omzet,
                    SUM(CASE WHEN win>0 THEN win ELSE 0 END) as win,
                    SUM(CASE WHEN lost< 0 THEN lost ELSE 0 END) as lost,
                    SUM(winlost_report) as win_lost
                    ')
                )->groupBy('sport_type')->first();
            $date->sport = $sport_type;

            //player
            $player = SumWinLoseModel::with('player')
                ->whereDate('date_trans', $date->date_trans)
                ->select(DB::raw(
                    'vendor_member_id,
                    SUM(total) as omzet,
                    SUM(CASE WHEN win>0 THEN win ELSE 0 END) as win,
                    SUM(CASE WHEN lost< 0 THEN lost ELSE 0 END) as lost,
                    SUM(winlost_report) as win_lost
                    ')
                )->groupBy('vendor_member_id')->first();
            $date->player = $player;
        }

        return response()->json(['message' => 'ok', 'data' => $dates], 200);
    }
}
