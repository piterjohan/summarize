<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\ServiceClass\Contract\SavesDataInterface;
use App\ServiceClass\TraitService\SaveTransBoping;

class BopingController extends Controller implements SavesDataInterface
{
    use SaveTransBoping;

    public function index()
    {
        try {
            $this->saveVersionKeyAndBopingTrans();

            return response()->json(['message' => "ok", 'data' => 'Recorded'],200);
        }catch (\Throwable $e){
            dd($e, 'error');
        }
    }


}
