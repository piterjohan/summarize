<?php

namespace App\ServiceClass\AbstractClass;

abstract class SavesBinding
{
    protected array $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    abstract function save();
}
