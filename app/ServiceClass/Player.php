<?php

namespace App\ServiceClass;


use App\Model\PlayerModel;
use App\ServiceClass\AbstractClass\SavesBinding;
use Carbon\Carbon;

class Player
{

    /**
     * Save Player from BetDetails & BetNumberDetails ,etc
     */
    public function save($vendor_member_id)
    {
        $have_member = PlayerModel::where('vendor_member_id', $vendor_member_id)->first();

        if (!$have_member) {
            PlayerModel::create([
                'vendor_member_id' => $vendor_member_id,
                'created_at' => Carbon::now(),
            ]);
        }
    }
}
