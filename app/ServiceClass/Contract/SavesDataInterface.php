<?php

namespace App\ServiceClass\Contract;

interface SavesDataInterface
{
    public function addressFetching($version_key);

    public function saveVersionKeyAndBopingTrans();

    public function saveTransBoping($index_key);
}
