<?php

namespace App\ServiceClass;

use App\Model\BopingTransModel;
use App\Model\PlayerModel;
use App\ServiceClass\AbstractClass\SavesBinding;
use App\ServiceClass\TraitService\SummarizeWinLose;

class History extends SavesBinding
{

    use SummarizeWinLose;
    /**
     * Save Data BetDetails & BetNumberDetails
     */
    public function save()
    {
        $trans_boping = [];

        foreach ($this->data as $value) {

            if ( !$this->checkBopingTransVersionKey($value['version_key']))
            {
                  $trans_boping[] = [
                    'trans_id' => $value['trans_id'],
                    'vendor_member_id' => $value['vendor_member_id'],
                    'operator_id' => $value['trans_id'],
                    'league_id' => (isset($value['league_id'])) ? $value['league_id'] : 0,
                    'leaguename' => (isset($value['leaguename'])) ? json_encode($value['leaguename']) : "",
                    'match_id' => $value['match_id'],
                    'home_id' => (isset($value['home_id'])) ? $value['home_id'] : 0,
                    'hometeamname' => (isset($value['hometeamname'])) ? json_encode($value['hometeamname']) : "",
                    'away_id' => (isset($value['away_id'])) ? $value['away_id'] : 0,
                    'awayteamname' => (isset($value['awayteamname'])) ? json_encode($value['awayteamname']) : "",
                    'team_id' => (isset($value['team_id'])) ? $value['team_id'] : 0,
                    'game_id' => (isset($value['game_id'])) ? $value['team_id'] : 0,
                    'match_datetime' => (isset($value['match_datetime'])) ? $value['match_datetime'] : null,
                    'sport_type' => $value['sport_type'],
                    'bet_type' => $value['bet_type'],
                    'parlay_ref_no' => (isset($value['parlay_ref_no'])) ? $value['parlay_ref_no'] : 0,
                    'odds' => $value['odds'],
                    'stake' => $value['stake'],
                    'transaction_time' => $value['transaction_time'],
                    'ticket_status' => $value['ticket_status'],
                    'winlost_amount' => $value['winlost_amount'],
                    'after_amount' => (isset($value['after_amount'])) ? $value['after_amount'] : 0,
                    'currency' => $value['currency'],
                    'winlost_datetime' => $value['winlost_datetime'],
                    'odds_type' => $value['odds_type'],
                    'odds_info' => (isset($value['odds_Info'])) ? $value['odds_Info'] : "",
                    'isLucky' => (isset($value['isLucky'])) ? $value['isLucky'] : "",
                    'bet_team' => (isset($value['bet_team'])) ? $value['bet_team'] : "",
                    'exculding' => (isset($value['exculding'])) ? $value['exculding'] : "",
                    'bet_tag' => (isset($value['bet_tag'])) ? $value['bet_tag'] : "",
                    'home_hdp' => (isset($value['home_hdp'])) ? $value['home_hdp'] : 0,
                    'away_hdp' => (isset($value['away_hdp'])) ? $value['away_hdp'] : 0,
                    'hdp' => (isset($value['hdp'])) ? $value['hdp'] : 0,
                    'ou_hdp' => (isset($value['ou_hdp'])) ? $value['ou_hdp'] : 0,
                    'betfrom' => $value['betfrom'],
                    'islive' => $value['islive'],
                    'home_score' => (isset($value['home_score'])) ? $value['home_score'] : 0,
                    'home_score2' => (isset($value['home_score2'])) ? $value['home_score2'] : 0,
                    'away_score' => (isset($value['away_score'])) ? $value['away_score'] : 0,
                    'away_score2' => (isset($value['away_score2'])) ? $value['away_score2'] : 0,
                    'settlement_time' => (isset($value['settlement_time'])) ? $value['settlement_time'] : null,
                    'customInfo1' => $value['customInfo1'],
                    'customInfo2' => $value['customInfo2'],
                    'customInfo3' => $value['customInfo3'],
                    'customInfo4' => $value['customInfo4'],
                    'customInfo5' => $value['customInfo5'],
                    'ba_status' => $value['ba_status'],
                    'esports_gameid' => (isset($value['esports_gameid'])) ? $value['esports_gameid'] : 0,
                    'total_score' => (isset($value['total_score'])) ? $value['total_score'] : 0,
                    'version_key' => $value['version_key'],
                    'parlayData' => (isset($value['ParlayData'])) ? json_encode($value['ParlayData']) : "",
                    'winlost' => $value['winlost_amount'],
                ];

                // insert player
                $player = new Player();
                $player->save($value['vendor_member_id']);
            }
        }

        // chunk
        $chunk_boping = array_chunk($trans_boping, 25);
        foreach ($chunk_boping as $chunk_data_val)
        {
            BopingTransModel::insert($chunk_data_val);
        }

    }

    /**
     * Calculate sum win
     */
    public function calculate()
    {
        //calculate amout by date
        $data_summarize = $this->sumarizeDataByDate();

        //save into sum winlost
        $this->saveIntoSumWin($data_summarize);
    }

    /**
     * check bopingTrans verion_key
     */
    protected function checkBopingTransVersionKey($version_key)
    {
        return BopingTransModel::where('version_key', $version_key)->first();
    }
}
