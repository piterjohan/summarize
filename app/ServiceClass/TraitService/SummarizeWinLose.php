<?php

namespace App\ServiceClass\TraitService;

use App\Model\BopingTransModel;
use App\Model\SumWinLoseModel;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Collection;

trait SummarizeWinLose
{
    /**
     * @param $date
     * @return Collection
     */
    protected function sumarizeDataByDate()
    {

        $group_by_date = BopingTransModel::select([DB::raw('
                    winlost_datetime,
                    vendor_member_id,
                    sum(stake) as omzet,
                    SUM(CASE WHEN winlost_amount>0 THEN winlost_amount ELSE 0 END) as win_amount,
                    SUM(CASE WHEN winlost_amount<0 THEN winlost_amount ELSE 0 END) as lose_amount,
                    sum(winlost_amount) as win_lost,
                    sport_type',),
        ])
            ->groupBy('winlost_datetime', 'sport_type' ,'vendor_member_id')
            ->get();

        return $group_by_date;
    }

    /**
     * save into table sumwin
     * @param Collection $data
     */
    protected function saveIntoSumWin($data)
    {

        foreach ($data as $result)
        {
            if( !$this->checkDataSumWin($result->winlost_datetime, $result->sport_type,$result->vendor_member_id) )
            {
                SumWinLoseModel::insert([
                    'date_trans' => $result->winlost_datetime,
                    'vendor_member_id' => $result->vendor_member_id,
                    'total' => $result->omzet,
                    'win' => $result->win_amount,
                    'lost' => $result->lose_amount,
                    'winlost_report' => $result->win_lost,
                    'sport_type' => $result->sport_type,
                ]);
            }else{
                SumWinLoseModel::whereDate('date_trans', $result->winlost_datetime)
                    ->where('sport_type', $result->sport_type)
                    ->where('vendor_member_id', $result->vendor_member_id)
                    ->update([
                        'date_trans' => $result->winlost_datetime,
                        'vendor_member_id' => $result->vendor_member_id,
                        'total' => $result->omzet,
                        'win' => $result->win_amount,
                        'lost' => $result->lose_amount,
                        'winlost_report' => $result->win_lost,
                        'sport_type' => $result->sport_type,
                    ]);
            }

        }
    }

    /**
     * checkDateSumWin
     * @param date $date_trans
     * @param int $sport_type
     * @param string $vendor_member_id
     * @return int 0 = not have data, 1 = have data
     */
    protected function checkDataSumWin($date_trans, $sport_type, $vendor_member_id)
    {
        return SumWinLoseModel::whereDate('date_trans', $date_trans)
            ->where('sport_type', $sport_type)
            ->where('vendor_member_id', $vendor_member_id)->first();
    }
}
