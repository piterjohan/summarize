<?php

namespace App\ServiceClass\TraitService;

use App\Jobs\BetHistory;
use App\Model\BopingTransModel;
use App\Model\VersionKeyModel;
use App\ServiceClass\ConstantKey;
use App\ServiceClass\History;
use App\ServiceClass\Player;
use App\ServiceClass\Versioning;
use Illuminate\Support\Facades\Http;

trait SaveTransBoping
{
    /**
     * Save version key (last_version_key)
     */
    public function saveVersionKeyAndBopingTrans()
    {
        $version_key = "";

        // get lastest version key
        $version_key_model = VersionKeyModel::get('version_key')->first();

        if( $version_key_model){ $version_key = $version_key_model->version_key; }

        while(true)
        {
            $json_data = $this->addressFetching($version_key);

            //update or create version key
            $save_version_key = new Versioning($json_data['Data']['last_version_key']);
            $save_version_key->versionKey();

            //check key response if just last version key skip
            (count($json_data['Data']) != 1)
                ? dispatch(new BetHistory($json_data))->onQueue('boping')
                : "";

            if ($version_key == $json_data['Data']['last_version_key']) {
                break;
            }
            $version_key = $json_data['Data']['last_version_key'];
        }
    }

    /**
     * addressFetching
     * @param int $version_key
     * @return json response
     */
    public function addressFetching($version_key)
    {
        $response = Http::asForm()->post('http://x9f2tsa.bw6688.com/api/GetBetDetail', [
            'vendor_id' => ConstantKey::VENDORID,
            'version_key' => $version_key,
        ]);
        return $response->json();
    }

    /**
     * Save transaction booping
     * @param String $index_key on fetching
     */
    public function saveTransBoping($index_key)
    {
        //save
        $bet_number_details = new History($this->data['Data'][$index_key]);

        $bet_number_details->save();
        $bet_number_details->calculate();
    }

}
