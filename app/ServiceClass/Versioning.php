<?php

namespace App\ServiceClass;

use App\Model\VersionKeyModel;
use Carbon\Carbon;

class Versioning
{
    protected int $version_key;

    /**
     * @param int $version_key
     */
    public function __construct(int $version_key)
    {
        $this->version_key = $version_key;
    }

    /**
     * Save Last Version Key
     */
    public function versionKey()
    {
        VersionKeyModel::updateOrCreate(
            ['id' => 1],
            ['version_key' => $this->version_key, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
    }
}
