<?php

namespace App\Jobs;

use App\ServiceClass\Contract\SavesDataInterface;
use App\ServiceClass\TraitService\SaveTransBoping;
use App\ServiceClass\Version;
use App\ServiceClass\VersionService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BetHistory implements ShouldQueue, SavesDataInterface
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, SaveTransBoping;

    protected array $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        foreach ($this->data['Data'] as $index_key => $data)
        {
            if( $index_key != 'last_version_key')
            {
                $this->saveTransBoping($index_key);
            }
        }

    }

}
