<?php

namespace App\Observers;

use App\Model\PlayerModel;
use App\ServiceClass\Player;
use App\User;
use Carbon\Carbon;
use Ramsey\Collection\Collection;

class UserObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
        if ( $this->checkPlayer($user->name))
        {
            PlayerModel::where('vendor_member_id', $user->name)->update([
                'vendor_member_id' => $user->name,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }else{
            PlayerModel::insert([
                'vendor_member_id' => $user->name,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }

    /**
     * checkPlayer
     * @param String $vendor_member_id
     * @return Collection
     */
    public function checkPlayer($vendor_member_id)
    {
        return PlayerModel::where('vendor_member_id', $vendor_member_id)->first();
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the user "restored" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
